package com.tddsample.tddsample.model;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "sexes", schema = "tddsample")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SexeNameEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "sexe_name_id")
    private Long id;

    private String sexeName;
}
