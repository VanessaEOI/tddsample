package com.tddsample.tddsample.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum SexeName {
    HOMME,
    FEMME,
    NEUTRE;
}
