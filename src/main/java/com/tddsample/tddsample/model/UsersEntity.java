package com.tddsample.tddsample.model;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Entity
@Table(name = "clients", schema = "tddsample")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UsersEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true, nullable = false)
    private String email;

    @Column(nullable = false)
    private String firstName;

    @Column(nullable = false)
    private String lastName;

    private String phoneNumber;

    private String birthDate;

    @Enumerated(EnumType.STRING)
    @ManyToOne
    @JoinColumn(name = "sexe_id")
    private List<SexeName> gender;

    @Column(columnDefinition = "boolean default true")
    private boolean isActive = true;
}
