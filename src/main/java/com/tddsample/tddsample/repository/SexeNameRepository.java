package com.tddsample.tddsample.repository;

import com.tddsample.tddsample.model.SexeNameEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SexeNameRepository extends JpaRepository<SexeNameEntity,Long> {
}
