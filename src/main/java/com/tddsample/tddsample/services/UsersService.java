package com.tddsample.tddsample.services;

import com.tddsample.tddsample.model.SexeName;
import com.tddsample.tddsample.model.UsersEntity;
import com.tddsample.tddsample.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UsersService {
    @Autowired
    private UsersRepository usersRepository;

    public String addUser(UsersEntity User) throws Exception {
        throw new Exception("Not implemented");
    }

    public List<UsersEntity> getAllUsers() throws Exception {
        throw new Exception("Not implemented");
    }

    public UsersEntity findUserById(Long id) throws Exception {
        throw new Exception("Not implemented");
    }

    public UsersEntity FindUserByEmail(String email) throws Exception {
        throw new Exception("Not implemented");
    }

    public List<UsersEntity> findAllUsersBySexe(SexeName sexe) throws Exception {
        throw new Exception("Not implemented");
    }

    public String deleteUser(Long id) throws Exception {
        throw new Exception("Not implemented");
    }

    public String disableUser(Long id) throws Exception {
        throw new Exception("Not implemented");
    }

    public String modifyUser(Long id) throws Exception {
        throw new Exception("Not implemented");
    }
}
